/*
  KDBLIFE
  ESR-CENTER POSITION_TEMP 테이블
*/

create table POSITION_TEMP
(
    POSITION_CODE varchar(32),
    POSITION_NAME varchar(100),
)
go

exec sp_addextendedproperty 'MS_Description', '직급코드', 'SCHEMA', 'dbo', 'TABLE', 'POSITION_TEMP', 'COLUMN', 'POSITION_CODE'
go

exec sp_addextendedproperty 'MS_Description', '직급명', 'SCHEMA', 'dbo', 'TABLE', 'POSITION_TEMP', 'COLUMN', 'POSITION_NAME'
go



/*
  KDBLIFE
  ESR-CENTER USER_TEMP 테이블
*/

create table USER_TEMP
(
    USER_ID varchar(32),
    USER_NAME varchar(32),
    USER_POSITION varchar(32),
    USER_DEPT varchar(32),
    USER_EMAIL varchar(32),
    IP varchar(32),
    MAC varchar(32),
    USER_CUSTOM03 varchar,
    SOURCE varchar(32)
)
go

exec sp_addextendedproperty 'MS_Description', '사번', 'SCHEMA', 'dbo', 'TABLE', 'USER_TEMP', 'COLUMN', 'USER_ID'
go

exec sp_addextendedproperty 'MS_Description', '사용자명', 'SCHEMA', 'dbo', 'TABLE', 'USER_TEMP', 'COLUMN', 'USER_NAME'
go

exec sp_addextendedproperty 'MS_Description', '사용자 직급', 'SCHEMA', 'dbo', 'TABLE', 'USER_TEMP', 'COLUMN', 'USER_POSITION'
go

exec sp_addextendedproperty 'MS_Description', '사용자 부서코드', 'SCHEMA', 'dbo', 'TABLE', 'USER_TEMP', 'COLUMN', 'USER_DEPT'
go

exec sp_addextendedproperty 'MS_Description', '사용자메일', 'SCHEMA', 'dbo', 'TABLE', 'USER_TEMP', 'COLUMN', 'USER_EMAIL'
go

exec sp_addextendedproperty 'MS_Description', '사용자 IP주소', 'SCHEMA', 'dbo', 'TABLE', 'USER_TEMP', 'COLUMN', 'IP'
go

exec sp_addextendedproperty 'MS_Description', '사용자 MAC주소', 'SCHEMA', 'dbo', 'TABLE', 'USER_TEMP', 'COLUMN', 'MAC'
go

exec sp_addextendedproperty 'MS_Description', '반출승인 결재권자 여부', 'SCHEMA', 'dbo', 'TABLE', 'USER_TEMP', 'COLUMN', 'USER_CUSTOM03'

exec sp_addextendedproperty 'MS_Description', '원천', 'SCHEMA', 'dbo', 'TABLE', 'USER_TEMP', 'COLUMN', 'SOURCE'
go


create table ESCORT_USER_TEMP
(
    USER_ID varchar(32),
    USER_NAME varchar(32),
    USER_POSITION varchar(32),
    USER_DEPT varchar(32),
    USER_EMAIL varchar(32),
    IP varchar(32),
    MAC varchar(32),
    USER_CUSTOM03 varchar,
    SOURCE varchar(32)
)
go

exec sp_addextendedproperty 'MS_Description', '사번', 'SCHEMA', 'dbo', 'TABLE', 'ESCORT_USER_TEMP', 'COLUMN', 'USER_ID'
go

exec sp_addextendedproperty 'MS_Description', '사용자명', 'SCHEMA', 'dbo', 'TABLE', 'ESCORT_USER_TEMP', 'COLUMN', 'USER_NAME'
go

exec sp_addextendedproperty 'MS_Description', '사용자 직급', 'SCHEMA', 'dbo', 'TABLE', 'ESCORT_USER_TEMP', 'COLUMN', 'USER_POSITION'
go

exec sp_addextendedproperty 'MS_Description', '사용자 부서코드', 'SCHEMA', 'dbo', 'TABLE', 'ESCORT_USER_TEMP', 'COLUMN', 'USER_DEPT'
go

exec sp_addextendedproperty 'MS_Description', '사용자메일', 'SCHEMA', 'dbo', 'TABLE', 'ESCORT_USER_TEMP', 'COLUMN', 'USER_EMAIL'
go

exec sp_addextendedproperty 'MS_Description', '사용자 IP주소', 'SCHEMA', 'dbo', 'TABLE', 'ESCORT_USER_TEMP', 'COLUMN', 'IP'
go

exec sp_addextendedproperty 'MS_Description', '사용자 MAC주소', 'SCHEMA', 'dbo', 'TABLE', 'ESCORT_USER_TEMP', 'COLUMN', 'MAC'
go

exec sp_addextendedproperty 'MS_Description', '반출승인 결재권자 여부', 'SCHEMA', 'dbo', 'TABLE', 'ESCORT_USER_TEMP', 'COLUMN', 'USER_CUSTOM03'

exec sp_addextendedproperty 'MS_Description', '원천', 'SCHEMA', 'dbo', 'TABLE', 'ESCORT_USER_TEMP', 'COLUMN', 'SOURCE'
go


/*
  KDBLIFE
  ESR-CENTER APRV_TEMP 테이블
*/

create table APRV_TEMP
(
    USER_ID  varchar(32),
    USER_NAME  varchar(32)
)
go

exec sp_addextendedproperty 'MS_Description', '사번', 'SCHEMA', 'dbo', 'TABLE', 'APRV_TEMP', 'COLUMN', 'USER_ID'
go

exec sp_addextendedproperty 'MS_Description', '사용자명', 'SCHEMA', 'dbo', 'TABLE', 'APRV_TEMP', 'COLUMN', 'USER_NAME'
go


/*
  KDBLIFE
  ESR-CENTER DEPT_TEMP 테이블
*/

create table DEPT_TEMP
(
    MADE_CODE  int,
    DEPT_CODE  varchar(32),
    DEPT_NAME  varchar(32),
    DEPT_PCODE varchar(32),
    SOURCE varchar(32)
)
go

exec sp_addextendedproperty 'MS_Description', '부서레벨', 'SCHEMA', 'dbo', 'TABLE', 'DEPT_TEMP', 'COLUMN', 'MADE_CODE'
go

exec sp_addextendedproperty 'MS_Description', '부서코드', 'SCHEMA', 'dbo', 'TABLE', 'DEPT_TEMP', 'COLUMN', 'DEPT_CODE'
go

exec sp_addextendedproperty 'MS_Description', '부서명', 'SCHEMA', 'dbo', 'TABLE', 'DEPT_TEMP', 'COLUMN', 'DEPT_NAME'
go

exec sp_addextendedproperty 'MS_Description', '상위부서코드', 'SCHEMA', 'dbo', 'TABLE', 'DEPT_TEMP', 'COLUMN',
     'DEPT_PCODE'

exec sp_addextendedproperty 'MS_Description', '원천', 'SCHEMA', 'dbo', 'TABLE', 'DEPT_TEMP', 'COLUMN',
     'SOURCE'
go

/*
NAC 부서 연동을 위해 필요한 NAC 부서 TEMP테이블
*/

create table NAC_DEPT_TEMP
(
    MADE_CODE  int,
    DEPT_CODE  varchar(32),
    DEPT_NAME  varchar(32),
    DEPT_PCODE varchar(32),
    SOURCE varchar(32)
)
go

exec sp_addextendedproperty 'MS_Description', '부서레벨', 'SCHEMA', 'dbo', 'TABLE', 'NAC_DEPT_TEMP', 'COLUMN', 'MADE_CODE'
go

exec sp_addextendedproperty 'MS_Description', '부서코드', 'SCHEMA', 'dbo', 'TABLE', 'NAC_DEPT_TEMP', 'COLUMN', 'DEPT_CODE'
go

exec sp_addextendedproperty 'MS_Description', '부서명', 'SCHEMA', 'dbo', 'TABLE', 'NAC_DEPT_TEMP', 'COLUMN', 'DEPT_NAME'
go

exec sp_addextendedproperty 'MS_Description', '상위부서코드', 'SCHEMA', 'dbo', 'TABLE', 'NAC_DEPT_TEMP', 'COLUMN',
     'DEPT_PCODE'

exec sp_addextendedproperty 'MS_Description', '원천', 'SCHEMA', 'dbo', 'TABLE', 'NAC_DEPT_TEMP', 'COLUMN',
     'SOURCE'
go


/*
  KDBLIFE
  ESCORT 사용자연동 시 필요한 View
*/

Create View [dbo].[V_Ext_OrgSrc_User]
As
Select empNo as user_id
        , 'pwd' as user_pwd,
       hName as user_nm,
       sDeptCode as dept_cd,
       HHP_No as user_mobile,
       isNull(Mail_Addr, '') as user_email,
       convert(varchar(10), getDate(), 120) as reg_dm,
       convert(varchar(10), getDate(), 120) as upd_dm,
       '1' as use_yn,
       'AutoSync' as regr_id,
       'ROLE_SUPER' as authority
From incops5.dbo.insaInfo
Where status not in ('DD', 'CC')
  and sDeptCode in (Select dept_cd from V_Ext_OrgSrc_Dept)
;


/*
  KDBLIFE
  ESCORT 부서연동 시 필요한 View
*/

Create View [dbo].[V_Ext_OrgSrc_Dept]
As
With dept( lvl, DeptCode, DeptName, HighDeptCode)
         As (Select 1 as lvl
                  ,DeptCode, DeptName, HighDeptCode
             From incops5.dbo.pc_deptCode
             Where DeptCode = 'TOP'
             Union All
             Select dept.lvl+1 as lvl
                  ,cDept.DeptCode,cDept.DeptName, cDept.HighDeptCode
             From incops5.dbo.pc_deptCode cDept, dept
             Where cDept.HighDeptCode = dept.DeptCode
    )
Select lvl as made_cd,
       DeptCode as dept_cd,
       DeptName as dept_nm,
       HighDeptCode as high_dept_cd
From dept;

/*
  KDBLIFE
  NAC POSITION_CODE가 'B030' OR USER_CUSTOM03이 '1'인 경우 APRV_AUTH_YN값을 'Y'로 업데이트 함
*/

alter table TUSER
    add APRV_AUTH_YN varchar
go


/*
    KDBLIFE 인사연동 시 필요한 Function
*/

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create Function [dbo].[F_DateTo14]( @pDate datetime)
    Returns char(14) -- YYYYMMDDHHmiss
AS
Begin

    Declare
        @dateTimeStr varchar(20)

    Select @dateTimeStr = Convert( varchar(20), @pDate, 120 );

    Set @dateTimeStr = replace(replace(replace(@dateTimeStr, ' ',''),'-',''),':','');

    Return @dateTimeStr;

End
GO