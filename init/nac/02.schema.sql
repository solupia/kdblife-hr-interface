

/*
  KDBLIFE
  NAC 부서 테이블
*/

create table ALDER.NAC_DeptView
(
    DEPT_CODE varchar(32) null comment '부서코드',
    DEPT_NAME varchar(32) null comment '부서명',
    DEPT_PCODE varchar(32) null comment '상위부서코드'
)
    comment 'NAC 부서';



/*
  KDBLIFE
  NAC 직급 테이블
*/

create table ALDER.NAC_PosView
(
    position_code varchar(32) null comment '직급코드',
    position_name varchar(100) null comment '직급명'
);

    comment 'NAC 직급'


/*
  KDBLIFE
  NAC 사용자 테이블
*/

create table ALDER.NAC_UserView
(
    USER_ID varchar(32) null comment '사번',
    USER_NAME varchar(32) null comment '이름',
    USER_POSITION varchar(32) null comment '직급',
    USER_DEPT varchar(32) null comment '사용자부서',
    USER_EMAIL varchar(32) null comment '사용자 메일',
    USER_CUSTOM03 varchar(32) null comment '결재권한'
)
    comment 'NAC 사용자';

alter table NAC_UserView
    add IP varchar(32) null comment 'IP주소';

alter table NAC_UserView
    add MAC varchar(32) null comment 'MAC주소';
