package lab.solupia.kdblife;

import lab.solupia.kdblife.api.CommonService;
import lab.solupia.kdblife.api.dept.DepartmentInterface;
import lab.solupia.kdblife.api.position.PositionInterface;
import lab.solupia.kdblife.api.user.UserInterface;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

@SpringBootApplication
@Slf4j
public class KdblifeApplication {

    public static void main(String[] args) {
        try (ConfigurableApplicationContext context =
                     SpringApplication.run(KdblifeApplication.class, args)) {
        }
    }

    @Bean
    @Profile({"development", "production", "staging", "productionext"})
    public CommandLineRunner runner(CommonService commonService, UserInterface userInterface, DepartmentInterface departmentInterface, PositionInterface positionInterface) {
        return (args -> {
            log.info("INSA SYNC START");
            commonService.truncateTempData();                 //Temp 테이블 데이터 삭제
            positionInterface.process();                      //Nac 직급 정보 Temp 테이블로 Insert(Escort에는 직급정보가 없으므로 Nac에서만 정보를 가지고온다)
            userInterface.process();                          //Nac, Escort 사용자 정보 Temp 테이블로 Merge(Nac 우선)
            departmentInterface.process();                    //Nac, Escort 부서 정보 Temp 테이블로 Merge(Nac 우선)
            if (commonService.checkTempTables()) {                // Temp 테이블 중 하나라도 데이터가 없으면 인사연동 시도하지 않음음
               commonService.synchronize();                      //Temp 테이블 데이터 연동
                commonService.renewDepartmentProperties();        //T_Org_DeptProp 테이블 갱신.
                commonService.insertApprovers();                  //직급코드가 B030 또는 USER_CUSTOM03 값이 1이면 결재권자로 추가함
                commonService.postProcess();                      //담당자 요청(인사연동 후처리)
            } else {
                log.info("TEMP TABLE DATA EMPTY");
            }
            log.info("INSA SYNC END");
        });
    }
}