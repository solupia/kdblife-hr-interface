package lab.solupia.kdblife.api;

import lab.solupia.kdblife.api.dept.service.DepartmentService;
import lab.solupia.kdblife.api.mapper.CommonMapper;
import lab.solupia.kdblife.api.position.service.PositionService;
import lab.solupia.kdblife.api.user.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author HONG (towz116@solupia.co.kr)
 * @since 2020-11-20(0020)
 */

@Slf4j
@Service
public class CommonService {

    private final DepartmentService departmentService;
    private final UserService userService;
    private final PositionService positionService;
    private final CommonMapper commonMapper;

    @Autowired
    public CommonService(DepartmentService departmentService, UserService userService, PositionService positionService, CommonMapper commonMapper) {
        this.departmentService = departmentService;
        this.userService = userService;
        this.positionService = positionService;
        this.commonMapper = commonMapper;
    }

    public void truncateTempData(){
        positionService.deletePositionTemp();
        userService.deleteUserTemp();
        userService.deleteEscortUserTemp();
        departmentService.deleteDepartmentTemp();
        departmentService.deleteNacDepartmentTemp();
        userService.deleteApproverTemp();
    }

    @Transactional
    public void synchronize(){
        positionService.synchronize();
        departmentService.synchronize();
        userService.synchronize();
    }

    @Transactional
    public void renewDepartmentProperties() {
        departmentService.deleteDepartmentProperties();
        departmentService.insertDepartmentProperties();
        departmentService.deleteDepartmentPropertiesGarbageData();
    }

    public void insertApprovers() {
        userService.insertApprovers();
    }

    public void postProcess() {
        log.info("미분류 부서 상위부서코드 사용 여부 수정");
        departmentService.updateNocategoryDepartmentCode();     //NAC에는 미분류부서가 존재하지 않아 후처리로 가용부서로 처리함
        log.info("TOP 부서코드 USE_YN 사용 안함");
        departmentService.updateTopDepartmentCode();            //NAC의 최상위부서코드는 '000000'이므로 기존의 TOP부서는 미사용부서로 처리함
    }

    public boolean checkTempTables() {
        int i = commonMapper.checkTempTables();
        return i == 0;
    }
}
