package lab.solupia.kdblife.api.config.datasource;


import com.zaxxer.hikari.HikariDataSource;
import lab.solupia.kdblife.api.config.datasource.prop.DatabaseProperties;
import lab.solupia.kdblife.api.config.datasource.prop.MasterDatabaseProperties;
import lab.solupia.kdblife.api.config.datasource.prop.NacDatabaseProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;

/**
 * @author HONG (towz116@solupia.co.kr)
 * @since 2020-11-20(0020)
 */
@Slf4j
public abstract class DataSourceConfig {
    DataSource makeDataSource(DatabaseProperties databaseProperties) {
        log.info("MAKE DATASOURCE: {}", databaseProperties.toString());

        return DataSourceBuilder.create().type(HikariDataSource.class)
                .driverClassName(databaseProperties.getDriverClassName())
                .url(databaseProperties.getUrl())
                .username(databaseProperties.getUsername())
                .password(databaseProperties.getPassword())
                .build();
    }
}

@Configuration
@EnableConfigurationProperties(MasterDatabaseProperties.class)
class MasterDatabaseConfig extends DataSourceConfig {

    private final MasterDatabaseProperties masterDatabaseProperties;

    @Autowired
    public MasterDatabaseConfig(MasterDatabaseProperties masterDatabaseProperties) {
        this.masterDatabaseProperties = masterDatabaseProperties;
    }

    @Primary
    @Bean(name = "masterDataSource")
    public DataSource dataSource() {
        return makeDataSource(masterDatabaseProperties);
    }
}

@Configuration
@EnableConfigurationProperties(NacDatabaseProperties.class)
class NacDatabaseConfig extends DataSourceConfig {

    private final NacDatabaseProperties nacDatabaseProperties;

    @Autowired
    public NacDatabaseConfig(NacDatabaseProperties nacDatabaseProperties) {
        this.nacDatabaseProperties = nacDatabaseProperties;
    }

    @Bean(name = "nacDataSource")
    public DataSource dataSource() {
        return makeDataSource(nacDatabaseProperties);
    }
}