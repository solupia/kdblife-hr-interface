package lab.solupia.kdblife.api.dept;

import lab.solupia.kdblife.api.dept.dto.DepartmentDTO;
import lab.solupia.kdblife.api.dept.service.DepartmentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author HONG (towz116@solupia.co.kr)
 * @since 2020-11-20(0020)
 */
@Component
@Slf4j
public class DepartmentInterface {
    private final DepartmentService departmentService;

    @Autowired
    public DepartmentInterface(DepartmentService departmentService) {
        this.departmentService = departmentService;
    }

    public void process() {
        log.info("*******************************************************************************************************************************");

        log.info("$$ ESCORT 부서 정보 연동..");
        List<DepartmentDTO> departmentList = departmentService.getEscortDepartments();
        log.info("$$ ESCORT 부서 COUNT ::: {}건", departmentList.size());

        log.info("$$ ESCORT 부서 정보 TEMP TABLE INSERT START");
        departmentService.insertEscortDepartmentTemp(departmentList);
        log.info("$$ ESCORT 부서 정보 TEMP TABLE INSERT END");

        log.info("$$ NAC 부서 정보 조회..");
        List<DepartmentDTO> nacDepartmentList = departmentService.getNacDepartments();
        log.info("$$ NAC 부서 COUNT :: {}건", nacDepartmentList.size());

        log.info("$$ NAC 부서 TEMP TABLE INSERT START");
        departmentService.insertNacDepartmentTemp(nacDepartmentList);
        log.info("$$ NAC 부서 TEMP TABLE INSERT END");

        log.info("$$ NAC 부서 TEMP TABLE LEVEL UPDATE START");
        departmentService.updateNacDepartmentTempLevel();
        log.info("$$ NAC 부서 TEMP TABLE LEVEL UPDATE END");

        log.info("$$ ESCORT 부서와 NAC 부서 정보 MERGE START");
        departmentService.mergeDepartmentsFromEscortAndNac();
        log.info("$$ ESOCRT 부서와 NAC 부서 정보 MERGE END");

        log.info("*******************************************************************************************************************************");
    }

}
