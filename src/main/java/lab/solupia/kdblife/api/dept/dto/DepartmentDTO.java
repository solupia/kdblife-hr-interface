package lab.solupia.kdblife.api.dept.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * @author HONG (towz116@solupia.co.kr)
 * @since 2020-11-20(0020)
 */
@Getter
@Setter
public class DepartmentDTO {
    private int madeCode;
    private String departmentCode;
    private String departmentName;
    private String parentDepartmentCode;

    @Override
    public String toString() {
        return "DepartmentDTO{" +
                "madeCode='" + madeCode + '\'' +
                "departmentCode='" + departmentCode + '\'' +
                ", departmentName='" + departmentName + '\'' +
                ", parentDepartmentCode='" + parentDepartmentCode + '\'' +
                '}';
    }
}
