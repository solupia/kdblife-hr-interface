package lab.solupia.kdblife.api.dept.dto;

import lombok.Getter;

/**
 * @author HONG (towz116@solupia.co.kr)
 * @since 2020-11-23(0023)
 */
@Getter
public class DepartmentPropertiesDTO {
    private String departmentNumber;
    private String departmentCode;
    private String codeId;
    private String stringValue;
    private Integer intValue;

    @Override
    public String toString() {
        return "DepartmentPropertiesDTO{" +
                "departmentNumber='" + departmentNumber + '\'' +
                ", departmentCode='" + departmentCode + '\'' +
                ", codeId='" + codeId + '\'' +
                ", stringValue='" + stringValue + '\'' +
                ", intValue=" + intValue +
                '}';
    }
}
