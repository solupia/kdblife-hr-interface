package lab.solupia.kdblife.api.dept.mapper;

import lab.solupia.kdblife.api.config.datasource.annotation.Master;
import lab.solupia.kdblife.api.dept.dto.DepartmentDTO;
import lab.solupia.kdblife.api.dept.dto.DepartmentPropertiesDTO;

import java.util.List;

/**
 * @author HONG (towz116@solupia.co.kr)
 * @since 2020-11-20(0020)
 */
@Master
public interface DepartmentMapper {
    void deleteDepartmentTemp();

    void deleteNacDepartmentTemp();

    void insertNacDepartmentTemp(DepartmentDTO department);

    void insertEscortDepartmentTemp(DepartmentDTO department);

    void mergeDepartmentsFromEscortAndNac();

    void updateRetiree();

    void updateServe();

    void insertNewDepartment();

    List<DepartmentDTO> getEscortDepartments();

    void deleteDepartmentProperties();

    List<DepartmentPropertiesDTO> getDepartmentProperties();

    void insertDepartmentProperties();

    void deleteDepartmentPropertiesGarbageData();

    void updateNacDepartmentTempLevel();

    void updateNocategoryDepartmentCode();

    void updateTopDepartmentCode();
}
