package lab.solupia.kdblife.api.dept.mapper;

import lab.solupia.kdblife.api.config.datasource.annotation.Nac;
import lab.solupia.kdblife.api.dept.dto.DepartmentDTO;

import java.util.List;

/**
 * @author HONG (towz116@solupia.co.kr)
 * @since 2020-11-20(0020)
 */
@Nac
public interface NacDepartmentMapper {

    List<DepartmentDTO> getNacDepartmentList();
}
