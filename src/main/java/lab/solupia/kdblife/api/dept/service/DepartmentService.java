package lab.solupia.kdblife.api.dept.service;

import lab.solupia.kdblife.api.dept.dto.DepartmentDTO;
import lab.solupia.kdblife.api.dept.dto.DepartmentPropertiesDTO;
import lab.solupia.kdblife.api.dept.mapper.DepartmentMapper;
import lab.solupia.kdblife.api.dept.mapper.NacDepartmentMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author HONG (towz116@solupia.co.kr)
 * @since 2020-11-20(0020)
 */
@Service
@Slf4j
public class DepartmentService {
    private final DepartmentMapper departmentMapper;
    private final NacDepartmentMapper nacDepartmentMapper;

    @Autowired
    public DepartmentService(DepartmentMapper departmentMapper, NacDepartmentMapper nacDepartmentMapper) {
        this.departmentMapper = departmentMapper;
        this.nacDepartmentMapper = nacDepartmentMapper;
    }

    public void deleteDepartmentTemp() {
        departmentMapper.deleteDepartmentTemp();
    }

    public void deleteNacDepartmentTemp() {
        departmentMapper.deleteNacDepartmentTemp();
    }

    public List<DepartmentDTO> getNacDepartments() {
        return nacDepartmentMapper.getNacDepartmentList();
    }

    public void insertNacDepartmentTemp(List<DepartmentDTO> departmentList) {
        for (DepartmentDTO department : departmentList) {
            departmentMapper.insertNacDepartmentTemp(department);
            log.info("NAC DEPARTMENT DATA ::: {}", department.toString());
        }

    }

    public void insertEscortDepartmentTemp(List<DepartmentDTO> departmentList) {
        for (DepartmentDTO department : departmentList) {
            departmentMapper.insertEscortDepartmentTemp(department);
            log.info("ESCORT DEPARTMENT DATA ::: {}", department.toString());
        }

    }

    public List<DepartmentDTO> getEscortDepartments() {
        List<DepartmentDTO> escortDepartmentList = departmentMapper.getEscortDepartments();

        for (DepartmentDTO department : escortDepartmentList) {
            log.info("ESCORT DEPARTMENT DATA ::: {}", department.toString());
        }
        return escortDepartmentList;
    }

    public void mergeDepartmentsFromEscortAndNac() {
        departmentMapper.mergeDepartmentsFromEscortAndNac();
    }

    public void synchronize() {
        log.info("부서 Data Synchronize START");
        departmentMapper.updateRetiree();     // 사라진부서확인 (삭제 예외 부서를 제외한 사라진 부서는 USE_YN 값을 0으로 설정함)
        log.info("부서 updateRetiree END");
        departmentMapper.updateServe();       // 존재하는부서확인 (Escort에서 사라졌더라도 ESR-Center에는 USE_YN 값을 1로 설정함)
        log.info("부서 updateServe END");
        departmentMapper.insertNewDepartment();     // 신규부서
        log.info("부서 insertNewDepartment END");
    }

    public void deleteDepartmentProperties() {
        List<DepartmentPropertiesDTO> departmentPropertiesList = departmentMapper.getDepartmentProperties();
        log.info("DELETE TARGET COUNT T_Org_DeptProp ::: {}건", departmentPropertiesList.size());

        for (DepartmentPropertiesDTO departmentProperties : departmentPropertiesList) {
            log.info("DELETE TARGET T_Org_DeptProp ::: {}", departmentProperties.toString());
        }

        if (departmentPropertiesList.size() > 0) {
            departmentMapper.deleteDepartmentProperties();
        }
    }

    public void insertDepartmentProperties() {
        departmentMapper.insertDepartmentProperties();
    }

    public void deleteDepartmentPropertiesGarbageData() {
        departmentMapper.deleteDepartmentPropertiesGarbageData();
    }

    public void updateNacDepartmentTempLevel() {
        departmentMapper.updateNacDepartmentTempLevel();
    }

    public void updateNocategoryDepartmentCode() {
        departmentMapper.updateNocategoryDepartmentCode();
    }

    public void updateTopDepartmentCode() {
        departmentMapper.updateTopDepartmentCode();
    }
}
