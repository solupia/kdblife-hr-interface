package lab.solupia.kdblife.api.mapper;

import lab.solupia.kdblife.api.config.datasource.annotation.Master;

/**
 * @author HONG (towz116@solupia.co.kr)
 * @since 2021-01-13(0013)
 */
@Master
public interface CommonMapper {
    int checkTempTables();
}
