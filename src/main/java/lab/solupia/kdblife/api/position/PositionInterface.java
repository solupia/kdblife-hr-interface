package lab.solupia.kdblife.api.position;

import lab.solupia.kdblife.api.position.dto.PositionDTO;
import lab.solupia.kdblife.api.position.service.PositionService;
import lab.solupia.kdblife.api.user.dto.UserDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author HONG (towz116@solupia.co.kr)
 * @since 2020-11-23(0023)
 */
@Component
@Slf4j
public class PositionInterface {
    private final PositionService positionService;

    @Autowired
    public PositionInterface(PositionService positionService) {
        this.positionService = positionService;
    }

    public void process() {
        log.info("*******************************************************************************************************************************");
        log.info("$$ 직급 정보 연동..");

        log.info("$$ NAC 직급 정보 조회..");
        List<PositionDTO> nacPositionList = positionService.getNacPositions();
        log.info("$$ NAC 직급 COUNT :: {}건", nacPositionList.size());

        log.info("$$ NAC 직급 정보 TEMP TABLE INSERT START");
        positionService.insertNacPositionTemp(nacPositionList);
        log.info("$$ NAC 직급 정보 TEMP TABLE INSERT END");

        log.info("*******************************************************************************************************************************");
    }
}
