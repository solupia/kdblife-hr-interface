package lab.solupia.kdblife.api.position.dto;

import lombok.Getter;

/**
 * @author HONG (towz116@solupia.co.kr)
 * @since 2020-11-23(0023)
 */
@Getter
public class PositionDTO {
    private String positionCode;
    private String positionName;

    @Override
    public String toString() {
        return "PositionDTO{" +
                "positionCode='" + positionCode + '\'' +
                ", positionName='" + positionName + '\'' +
                '}';
    }
}
