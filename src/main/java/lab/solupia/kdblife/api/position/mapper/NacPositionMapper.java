package lab.solupia.kdblife.api.position.mapper;

import lab.solupia.kdblife.api.config.datasource.annotation.Nac;
import lab.solupia.kdblife.api.position.dto.PositionDTO;

import java.util.List;

/**
 * @author HONG (towz116@solupia.co.kr)
 * @since 2020-11-23(0023)
 */

@Nac
public interface NacPositionMapper {
    List<PositionDTO> getNacPositions();
}
