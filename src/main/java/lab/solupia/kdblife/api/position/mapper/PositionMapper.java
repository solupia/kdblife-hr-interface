package lab.solupia.kdblife.api.position.mapper;

import lab.solupia.kdblife.api.config.datasource.annotation.Master;
import lab.solupia.kdblife.api.position.dto.PositionDTO;

/**
 * @author HONG (towz116@solupia.co.kr)
 * @since 2020-11-23(0023)
 */
@Master
public interface PositionMapper {
    void insertNacPositionTemp(PositionDTO position);

    void deletePositionTemp();

    void mergePositions();
}
