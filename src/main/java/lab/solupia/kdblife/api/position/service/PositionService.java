package lab.solupia.kdblife.api.position.service;

import lab.solupia.kdblife.api.position.dto.PositionDTO;
import lab.solupia.kdblife.api.position.mapper.NacPositionMapper;
import lab.solupia.kdblife.api.position.mapper.PositionMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author HONG (towz116@solupia.co.kr)
 * @since 2020-11-23(0023)
 */

@Service
@Slf4j
public class PositionService {
    private final NacPositionMapper nacPositionMapper;
    private final PositionMapper positionMapper;

    @Autowired
    public PositionService(NacPositionMapper nacPositionMapper, PositionMapper positionMapper) {
        this.nacPositionMapper = nacPositionMapper;
        this.positionMapper = positionMapper;
    }

    public void deletePositionTemp() {
        positionMapper.deletePositionTemp();
    }

    public List<PositionDTO> getNacPositions() {
        return nacPositionMapper.getNacPositions();
    }

    public void insertNacPositionTemp(List<PositionDTO> nacPositionList) {
        for (PositionDTO position : nacPositionList) {
            positionMapper.insertNacPositionTemp(position);
            log.info("NAC POSITION DATA ::: {}", position.toString());
        }
    }

    public void synchronize() {
        log.info("직급 정보 동기화 START");
        positionMapper.mergePositions();
        log.info("직급 정보 동기화 END");
    }
}
