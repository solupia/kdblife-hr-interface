package lab.solupia.kdblife.api.user;

import lab.solupia.kdblife.api.user.dto.UserDTO;
import lab.solupia.kdblife.api.user.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author HONG (towz116@solupia.co.kr)
 * @since 2020-11-20(0020)
 */
@Component
@Slf4j
public class UserInterface {
    private final UserService userService;

    @Autowired
    public UserInterface(UserService userService) {
        this.userService = userService;
    }

    public void process() {
        log.info("*******************************************************************************************************************************");

                log.info("$$ 사용자 정보 연동..");

                log.info("$$ NAC 사용자 정보 조회..");
                List<UserDTO> nacUserList = userService.getNacUsers();
                log.info("$$ NAC 사용자 COUNT :: {}건", nacUserList.size());

                log.info("$$ NAC 사용자 정보 TEMP TABLE INSERT START");
                userService.insertNacUserTemp(nacUserList);
                log.info("$$ NAC 사용자 정보 TEMP TABLE INSERT END");

                log.info("$$ ESCORT 사용자 정보 조회..");
                List<UserDTO> escortUserList = userService.getEscortUsers();
                log.info("$$ ESCORT 사용자 COUNT :: {}건", escortUserList.size());

                userService.insertEscortUserTemp(escortUserList);

                log.info("$$ NAC 사용자와 에스코트 사용자 정보 MERGE START");
                userService.mergeUsersFromEscortAndNac();
                log.info("$$ NAC 사용자와 에스코트 사용자 정보 MERGE END");

        log.info("*******************************************************************************************************************************");
    }
}
