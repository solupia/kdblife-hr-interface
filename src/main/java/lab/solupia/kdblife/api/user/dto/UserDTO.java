package lab.solupia.kdblife.api.user.dto;

import lombok.Getter;

/**
 * @author HONG (towz116@solupia.co.kr)
 * @since 2020-11-20(0020)
 */

@Getter
public class UserDTO {
    private String userId;
    private String userName;
    private String userPositionCode;
    private String userDepartmentNumber;
    private String userDepartmentCode;
    private String userEmail;
    private String approverValue;
    private String ipAddress;
    private String macAddress;

    @Override
    public String toString() {
        return "UserDTO{" +
                "userId='" + userId + '\'' +
                ", userName='" + userName + '\'' +
                ", userPositionCode='" + userPositionCode + '\'' +
                ", userDepartmentCode='" + userDepartmentCode + '\'' +
                ", userDepartmentNumber='" + userDepartmentNumber + '\'' +
                ", userEmail='" + userEmail + '\'' +
                ", approverValue='" + approverValue + '\'' +
                ", ipAddress='" + ipAddress + '\'' +
                ", macAddress='" + macAddress + '\'' +
                '}';
    }
}
