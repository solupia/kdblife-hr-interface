package lab.solupia.kdblife.api.user.mapper;

import lab.solupia.kdblife.api.config.datasource.annotation.Nac;
import lab.solupia.kdblife.api.user.dto.UserDTO;

import java.util.List;

/**
 * @author HONG (towz116@solupia.co.kr)
 * @since 2020-11-20(0020)
 */
@Nac
public interface NacUserMapper {
    List<UserDTO> getNacUserList();
}
