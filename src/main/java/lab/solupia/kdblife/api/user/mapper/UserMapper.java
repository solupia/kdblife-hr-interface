package lab.solupia.kdblife.api.user.mapper;

import lab.solupia.kdblife.api.config.datasource.annotation.Master;
import lab.solupia.kdblife.api.user.dto.UserDTO;

import java.util.List;

/**
 * @author HONG (towz116@solupia.co.kr)
 * @since 2020-11-20(0020)
 */
@Master
public interface UserMapper {
    void deleteUserTemp();

    void deleteEscortUserTemp();

    void insertNacUserTemp(UserDTO user);

    void mergeUsersFromEscortAndNac();

    List<UserDTO> getEscortUsers();

    void updateRetiree();

    void updateServe();

    void insertNewUser();

    List<UserDTO> getApprovers();

    void insertApproverTemp(UserDTO approver);

    void mergeApprover();

    void deleteApprovers();

    void insertEscortUserTemp(UserDTO escortUser);

    void deleteApproverTemp();

    void moveToNocategoryUser();

    void updateDefaultUserPosition();
}
