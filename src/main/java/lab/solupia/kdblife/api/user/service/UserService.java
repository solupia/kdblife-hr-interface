package lab.solupia.kdblife.api.user.service;

import lab.solupia.kdblife.api.user.dto.UserDTO;
import lab.solupia.kdblife.api.user.mapper.NacUserMapper;
import lab.solupia.kdblife.api.user.mapper.UserMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author HONG (towz116@solupia.co.kr)
 * @since 2020-11-20(0020)
 */
@Service
@Slf4j
public class UserService {
    private final UserMapper userMapper;
    private final NacUserMapper nacUserMapper;


    @Autowired
    public UserService(UserMapper userMapper, NacUserMapper nacUserMapper) {
        this.userMapper= userMapper;
        this.nacUserMapper = nacUserMapper;
    }

    public void deleteUserTemp(){
        userMapper.deleteUserTemp();
    }

    public void deleteEscortUserTemp(){
        userMapper.deleteEscortUserTemp();
    }

    public List<UserDTO> getNacUsers() {
        return nacUserMapper.getNacUserList();
    }

    public void insertNacUserTemp(List<UserDTO> userList) {
        for (UserDTO user : userList) {
            userMapper.insertNacUserTemp(user);
            log.info("NAC USER DATA ::: {}", user.toString());
        }
    }

    public void mergeUsersFromEscortAndNac() {
        userMapper.mergeUsersFromEscortAndNac();
    }

    public List<UserDTO> getEscortUsers() {
        List<UserDTO> escortUserList = userMapper.getEscortUsers();

        for (UserDTO user : escortUserList) {
            log.info("ESCORT USER DATA ::: {}", user.toString());
        }
        return escortUserList;
    }

    public void synchronize() {
        log.info("사용자 Data Synchronize START");
        userMapper.updateRetiree();     // 퇴사확인
        log.info("사용자 updateRetiree END");
        userMapper.updateServe();       // 재직확인
        log.info("사용자 updateServe END");
        userMapper.insertNewUser();     // 신규사용자
        log.info("사용자 insertNewUser END");
    }

    public void insertApprovers() {
        log.info("INSERT APPROVERS START");

        userMapper.deleteApprovers();   //NAC 기준 데이터로 결재권한이 있는 사람들로만 관리하기위해 기존 데이터를 삭제함

        List<UserDTO> approvers = userMapper.getApprovers();
        log.info(" $$$ NAC APPROVERS COUNT ::: {}건", approvers.size());

        for (UserDTO approver : approvers) {
            log.info(" $$$ NAC APPROVER ::: {}",approver.toString());
            userMapper.insertApproverTemp(approver);
        }
        log.info("INSERT APPROVERS END");

        userMapper.mergeApprover();
    }

    public void insertEscortUserTemp(List<UserDTO> escortUserList) {
        for (UserDTO escortUser : escortUserList) {
            userMapper.insertEscortUserTemp(escortUser);
        }

    }

    public void moveToNocategoryUser() {
        userMapper.moveToNocategoryUser();
    }

    public void deleteApproverTemp() {
        userMapper.deleteApproverTemp();
    }

    public void updateDefaultUserPosition() {
        userMapper.updateDefaultUserPosition();
    }

}
